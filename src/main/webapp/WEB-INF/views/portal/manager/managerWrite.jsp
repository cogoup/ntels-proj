<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="eng" lang="eng">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="Content-Script-Type" content="text/javascript" />
<meta http-equiv="Content-Style-Type" content="text/css" />
<title>Manager</title>
<link rel="stylesheet" type="text/css" href="/css/default.css" />	
<link rel="stylesheet" type="text/css" href="/css/guide.css" />	
<link rel="stylesheet" type="text/css" href="/css/content.css" />	
<script type="text/javascript" src="/scripts/jquery-ui/jquery.min.js"></script>
<script type="text/javascript" src="/scripts/common/common-ui.js"></script>
<style type="text/css">
.checkID{
    padding: 3px;
    background-color: #8a8c8e;
    border: 0px;
    color: white;
    margin-left: 10px;
    width: 80px;
}
</style>
</head>
<body>
<div id="wrap">
    <!-- header -->
    <div id="header"></div>
    <!-- header -->

    <!-- container -->
    <div id="container">

		<!-- lnb -->
		<div class="lnb"></div>
		<!-- lnb -->

		<!-- contents -->
    	<div class="contents">

			<!-- location -->
            <div class="location"><a href="#">Home</a> &gt; <a href="#">Admin</a> &gt; <a href="#"><span class="txt_w">Manager</span></a></div> 
			<!-- location -->

			<!-- table -->	
          	<div class="title">
                <span class="brd_rtop mgb10">
					<span class="btn_orange"><a href="/portal/managerList">List</a></span>
                </span>
            </div>

			<!-- table_detail -->
			<form id="frm" action="/portal/writeManagerInfo" method="post">
				<table class="brd_detail">
					<caption>Manager Write</caption>
					<colgroup>
						<col width="25%" />
						<col width="75%" />
					</colgroup>
					<tbody>
						<tr>
							<th>ID</th>
							<!--location.href='/portal/idCheck?userId='$('.userId').val()''  -->
							<td><input type="text" name="userId" class="userId" style="width:188px;"/><button type="button" class="checkID"> Check ID </button><span id="idChk"
							style="color: orange;font-weight: 900;padding: 10px;"></span></td>
						</tr>
						<tr>	
							<th>Manager Name <img src="/images/mone/star.gif" alt=""/></th>
							<td><input name="userName" type="text" class="" style="width:188px;"/></td>
						</tr>
						<tr>
							<th>Manager Group <img src="/images/mone/star.gif" alt=""/></th>
							<td>
								<select name="userGroupName" style="width:200px;">
									<c:forEach items="${gList}" var="g" varStatus="i">
										<option value="${g.userGroupName}">${g.userGroupName}</option>
									</c:forEach>							
								</select>
							</td>
						</tr>
						<tr>
							<th>Password <img src="/images/mone/star.gif" alt=""/></th>
							<td><input name="password" type="password" class="pw1" style="width:188px;"/></td>
						</tr>
						<tr>
							<th>Retype Password <img src="/images/mone/star.gif" alt=""/></th>
							<td><input name="rePassword" type="password" class="pw2" style="width:188px;"/><span id="pwChk"
							style="color: orange;font-weight: 900;padding: 10px;"></span></td>
						</tr>
						<tr>
							<th>Department</th>
							<td><input name="deptName" type="text" class="" style="width:188px;"/></td>
						</tr>
						<tr>
							<th>Employee No.</th>
							<td><input name="empNo" type="text" class="" style="width:188px;"/></td>
						</tr>
						<tr>
							<th>Tel.</th>
							<td><input name="telNo" type="text" class="" style="width:188px;"/></td>
						</tr>
						<tr>
							<th>E-mail <img src="/images/mone/star.gif" alt=""/></th>
							<td><input name="email" type="text" class="" style="width:188px;"/></td>
						</tr>
						<tr>
							<th>Status</th>
							<td>
								<select name="userYN" style="width:200px;">
									<option>Y</option>
									<option>N</option>
								</select>
							</td>
						</tr>
					</tbody>
				</table>
			</form>
            <div class="brd_btn">
				<span class="left">
					<span class="btn_gray2"><a href="#" id="btn_reset" value="Reset">Reset</a></span>
				</span>
				<span class="right">
					<span class="btn_orange"><a href="#" onclick="document.getElementById('frm').submit();">Save</a></span>
				</span>
            </div>		
			<!-- table_detail -->

		</div>	 
		<!-- contents -->
		
	</div>
    <!-- container -->
   
    <!-- footer -->
    <div id="footer"></div>
    <!-- footer -->

</div>
<script>
$(".checkID").click(function checkId(){
	if (/^[0-9a-z]+$/g.test($('[name=userId]').val())) {
		var id = $(".userId").val();
		$.ajax({
			url : "/portal/idCheck",
			data : {
				userId:id
			},
			type : "post",
			success : function(data) {
				if (data == 1) {
					alert("이미 사용중인 아이디 입니다.");
					$("#idChk").html("다시 입력해주세요.");
					
				} else {
					alert("사용가능한 아이디 입니다.");
					$("#idChk").html("사용가능한 아이디 입니다.");
				}
			}
		})	
	} else {
		$('#idChk')
				.html("숫자, 영문만 입력 가능");
	}
});
$('[name=rePassword]').focusout(function () {
    var pwd1 = $(".pw1").val();
    var pwd2 = $(".pw2").val();
    if ( pwd1 != '' && pwd2 == '' ) {
        null;
    } else if (pwd1 != "" || pwd2 != "") {
        if (pwd1 == pwd2) {
            alert('일치');
        } else {
        	$('.pw2').val('');
        	alert('불일치');
			$("#pwChk").html("다시 입력해주세요.");
			
        }
    }
});
$('[id=btn_reset]').click(function () {
	 $( "#frm" ).each( function () {
         this.reset();
     });
});
</script>
</body>
</html>
