<%@ page contentType="text/html;charset=UTF-8"%>
<%@ page pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<!DOCTYPE html>
<html>
<body>
<head>
<meta charset="utf-8" />
</style>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="Content-Script-Type" content="text/javascript" />
<meta http-equiv="Content-Style-Type" content="text/css" />
<title>Manager</title>
<link rel="stylesheet" type="text/css" href="/css/default.css" />	
<link rel="stylesheet" type="text/css" href="/css/guide.css" />	
<link rel="stylesheet" type="text/css" href="/css/content.css" />	
<script type="text/javascript" src="/scripts/jquery-ui/jquery.min.js"></script>
<script type="text/javascript" src="/scripts/common/common-ui.js"></script>
<style type="text/css">
ul.menu{
	margin-top:10px;
}
ul.menu>li{
	border-bottom: 2px solid #cbc5c5;
}
ul.menu li, ol.menu li {
    padding: 5px 0px 5px 13px;
    margin-bottom: 5px;
    font-size: 14px;
}
ul.menu li:last-child,
ol.menu li:last-child {
    border-bottom: 0px;
}
li.childList{
	color : lightgray;
	margin-left:20px;
	font-size:15px;
}
</style>
</head>
<body>
<div id="wrap">
    <!-- header -->
    <div id="header"></div>
    <!-- header -->

    <!-- container -->
    <div id="container">

		<!-- lnb -->
		<div class="lnb">
			<ul class="menu">
	           <li class="admin">Admin</li>
	           <ul>
	           	<li>Menu Manager</li>
       			<li class="childList" onclick="ttt()" onmouseover=this.style.color='orange' onmouseout=this.style.color='lightgray'>Manager</li>
		       	</ul>
      		</ul>
		</div>
		<!-- lnb -->

		<!-- contents -->
    	<div class="contents">

			<!-- location -->
            <div class="location"><a href="#">Home</a> &gt; <a href="#">Admin</a> &gt; <a href="/portal/managerList"><span class="txt_w">Manager</span></a></div> 
			<!-- location -->

          	<div class="title">
                <span class="brd_rtop mgb10">
					<span class="btn_orange"><a href="/portal/managerWrite">Add New Manager</a></span>
                </span>
            </div>

			<!-- search -->
			<div class="search">
				<strong>Manager Group</strong> 
				<select name="" style="width:180px;">
					<c:forEach items="${gList}" var="g" varStatus="i">
						<option value="${g.userGroupName}">${g.userGroupName}</option>
					</c:forEach>
				</select>
				<strong class="mgl50">Status</strong> 
				<select name="" style="width:180px;">
					<option>Y</option>
					<option>N</option>
				</select><br />
				<strong>Factor Name</strong> 
				<select name="" style="width:180px;">
					<option>Login ID</option>
					<option>User Name</option>
					<option>Employee No.</option>
				</select>	
				<input name="" type="text" class="keyword" title="keyword" style="width:238px;" />
				<span class="btn_search"><a href="#" onclick="searchManager(this);">Search</a></span>
			</div>
			<!-- search -->	

			<!-- table -->	
          	<div class="title mgt20">
				<h3>Manager List</h3>
                <span class="brd_rtop">
					Total List <span class="num">
					${fn:length(list)}
                </span>
            </div>

			<table class="brd">
				<caption>Manager List</caption>
				<colgroup>
					<col width="10%" />
					<col width="*" />
					<col width="20%" />
					<col width="12%" />
					<col width="20%" />
					<col width="10%" />
				</colgroup>
				<thead>
					<tr>
						<th>ID</th>
						<th>Name</th>
						<th>Manager Group</th>
						<th>Tel.</th>
						<th>E-mail</th>
						<th>Edit</th>
					</tr>
				</thead>
				
				<c:forEach items="${viewAll}" var="manager" varStatus="i">
					<tbody>
						<tr>
							<td>${manager.userId}</td>
							<td>${manager.userName}</td>
							<td>${manager.userGroupName}</td>
							<td>${manager.telNo}</td>
							<td>${manager.email}</td>  
							<td><span class="btn_s_green"><a href="#" onclick="updateManager(this);">Edit</a></span></td>
						</tr>
					</tbody>
				</c:forEach>
			</table>
			<!-- table -->	

			<!-- paging -->
			<div class="paging">
				<c:if test="${paging.startPage != 1 }">
					<a href="/portal/managerList?nowPage=${paging.startPage - 1 }&cntPerPage=${paging.cntPerPage}"><img src="/images/mone/btn_paging_first.gif" alt="first" /></a>
				</c:if>	
				<c:forEach begin="${paging.startPage }" end="${paging.endPage }" var="p">
					<c:choose>
						<c:when test="${p == paging.nowPage }">
							<span class="page_num"><a class="on">${p }</a></span>
						</c:when>
						<c:when test="${p != paging.nowPage }">
							<span class="page_num"><a href="/portal/managerList?nowPage=${p}&cntPerPage=${paging.cntPerPage}">${p}</a></span>
						</c:when>
					</c:choose>
				</c:forEach>	
				<c:if test="${paging.endPage != paging.lastPage}">
					<a href="/portal/managerList?nowPage=${paging.endPage+1 }&cntPerPage=${paging.cntPerPage}"><img src="/images/mone/btn_paging_next.gif" alt="next" /></a>
				</c:if>
			</div>
			<!-- paging -->
			
		</div>	 
		<!-- contents -->
		
	</div>
    <!-- container -->
   
    <!-- footer -->
    <div id="footer"></div>
    <!-- footer -->

</div>
<script type="text/javascript">
	$(document).on('click', '.childList', function(e){
	    e.preventDefault();  
		location.href = "/portal/managerList";
	})
</script>
<script>
function searchManager(data){
	//userGroupName 선택
	var group = $(data).parent().parent().children().eq(1).find("option:selected").val();
	//사용여부 - Y : 활동 N : 비활동
	var YN = $(data).parent().parent().children().eq(3).find("option:selected").val();
	//LoginId(userId),userName(userName),부서이름(userGroupName])
	var sel = $(data).parent().parent().children().eq(6).find("option:selected").val();
	//검색내용
	var search = $(data).parent().parent().children().eq(7).val();
	var form = {
			userGroupName: group,
			userYN: YN,
			select : sel,
			keyword : search
    }
	$.ajax({
         url: "/portal/searchManagerList",
         type: "GET",
         data: form,
         success: function(data){
        	 
        	 $("tbody").empty();
        	 var tbody = $("<tbody>");
        	 for(var i=0;i<data.length;i++){
				var tr = $("<tr>");
				tr.append("<td>"+data[i].userId+"</td>");
				tr.append("<td>"+data[i].userName+"</td");
				tr.append("<td>"+data[i].userGroupName+"</td");
				tr.append("<td>"+data[i].telNo+"</td>");
				tr.append("<td>"+data[i].email+"</td>");
				tr.append("<td><span class='btn_s_green'><a href='/portal/managerUpdate'>Edit</a></span></td>");
				tbody.append(tr);						
			};
			$(".brd").append(tbody);
			$(".num").html(data.length);
         },
         error: function(){
             alert("err");
         }
     });
}
function updateManager(data){
	var id = $(data).parent().parent().parent().children().eq(0).html();
	var managerGroup = $(data).parent().parent().parent().children().eq(2).html();
	location.href = "/portal/managerUpdate?userId="+id+"&userGroupName="+managerGroup;
}
</script>
</body>
</html>