<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
    
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="eng" lang="eng">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="Content-Script-Type" content="text/javascript" />
<meta http-equiv="Content-Style-Type" content="text/css" />
<title>Manager</title>
<link rel="stylesheet" type="text/css" href="/css/default.css" />	
<link rel="stylesheet" type="text/css" href="/css/guide.css" />	
<link rel="stylesheet" type="text/css" href="/css/content.css" />	
<script type="text/javascript" src="/scripts/jquery-ui/jquery.min.js"></script>
<script type="text/javascript" src="/scripts/common/common-ui.js"></script>
<style type="text/css">
</style>
</head>
<body>
<div id="wrap">
    <!-- header -->
    <div id="header"></div>
    <!-- header -->

    <!-- container -->
    <div id="container">

		<!-- lnb -->
		<div class="lnb"></div>
		<!-- lnb -->

		<!-- contents -->
    	<div class="contents">

			<!-- location -->
            <div class="location"><a href="#">Home</a> &gt; <a href="#">Admin</a> &gt; <a href="#"><span class="txt_w">Manager</span></a></div> 
			<!-- location -->

			<!-- table -->	
          	<div class="title">
                <span class="brd_rtop mgb10">
					<span class="btn_orange"><a href="/portal/managerList">List</a></span>
                </span>
            </div>
			<form id="frm" action="/portal/managerUpdateInfo" method="post">
			<!-- table_detail -->
			<table class="brd_detail">
				<caption>Manager Update</caption>
				<colgroup>
					<col width="25%" />
					<col width="75%" />
				</colgroup>
				
				<tbody>
					<tr>
						<th>ID</th>
						<td>${m.userId}<input type="hidden" name="userId" value="${m.userId}"/></td>
					</tr>
					<tr>
						<th>Manager Name <img src="/images/mone/star.gif" alt=""/></th>
						<td><input name="userName" value="${m.userName}" type="text" class="" style="width:188px;"/></td>
					</tr>
					<tr>
						<th>Manager Group <img src="/images/mone/star.gif" alt=""/></th>
						<td>
							<select name="userGroupName" style="width:200px;">
							<option value="${m.userGroupName}" selected>${m.userGroupName}</option>
								<c:forEach items="${gList}" var="g" varStatus="i">
									<option value="${g.userGroupName}">${g.userGroupName}</option>
								</c:forEach>
							</select>
						</td>
 						</tr>
					<tr>
						<th>Department</th>
						<td><input name="deptName" value="${m.deptName}" type="text" class="" style="width:188px;"/></td>
					</tr>
					<tr>
						<th>Employee No.</th>
						<td><input name="empNo" value="${m.empNo}" type="text" class="" style="width:188px;"/></td>
					</tr>
					<tr>
						<th>Tel.</th>
						<td><input name="telNo" value="${m.telNo}" type="text" class="" style="width:188px;"/></td>
					</tr>
					<tr>
						<th>E-mail <img src="/images/mone/star.gif" alt=""/></th>
						<td><input name="email" value="${m.email}" type="text" class="" style="width:188px;"/></td>
					</tr>
					<tr>
						<th>Status</th>
						<td>
							<select name="userYN" style="width:200px;">
								<option>Y</option>
								<option>N</option>
							</select>
						</td>
					</tr>
				</tbody>
			</table>
			</form>
            <div class="brd_btn">
				<span class="right">
					<span class="btn_orange"><a href="#" onclick="document.getElementById('frm').submit();">Edit</a></span>
				</span>
            </div>		
			<!-- table_detail -->

		</div>	 
		<!-- contents -->
		
	</div>
    <!-- container -->
   
    <!-- footer -->
    <div id="footer"></div>
    <!-- footer -->

</div>
</body>
</html>