<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
 <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
 <c:set var="contextPath" value="${pageContext.request.contextPath}"/>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>JSONTest</title>
<script src="http://code.jquery.com/jquery-latest.js"></script>
<script>
	$(function(){
		$("#checkJson").click(function(){
			var info = {
				userGroupName : "Root Admin",
				gGroupId : "001",
				userCount : 6
			};
			$.ajax({
				type:"post",
				url:"/api/user-count",
				contentType:"application/json",
				data : JSON.stringify(info),
				success:function(data,textStatus){
				 	alert("성공");
				},
				error: function(data,textStatus){
					alert("에러가 발생했습니다."); 
				},
				complete : function(data,textStatus){
					
				}
			});
		});
	});
</script>
</head>
<body>
	<input type="button" id="checkJson" value="보내기"/>
</body>
</html>