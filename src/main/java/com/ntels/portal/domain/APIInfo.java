package com.ntels.portal.domain;

import java.util.ArrayList;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;


@Data
public class APIInfo {
	public APIInfo() {}
	private String userGroupName;
	private String userGroupId;
	private int userCount;
}
