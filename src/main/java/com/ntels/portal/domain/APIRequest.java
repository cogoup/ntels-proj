package com.ntels.portal.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
public class APIRequest {
	public APIRequest() {};
	private String userGroupName;
	private String userGroupId;
	private int userCount;
}
