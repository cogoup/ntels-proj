package com.ntels.portal.dao;

import java.util.ArrayList;
import java.util.List;

import org.mybatis.spring.SqlSessionTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.ntels.portal.domain.Manager;

@Repository
public class ManagerMapper {
	@Autowired
	private SqlSessionTemplate sqlSession;
	
	public ArrayList<Manager> ManagerList() {
		List<Manager> list = (List<Manager>) sqlSession.selectList("manager.selectManagerList");
		return (ArrayList<Manager>)list;
	}

	public ArrayList<Manager> groupList() {
		List<Manager> list = (List<Manager>) sqlSession.selectList("manager.groupList");
		return (ArrayList<Manager>)list;
	}

	public ArrayList<Manager> searchManagerList(Manager manager) {
		List<Manager> list = (List<Manager>) sqlSession.selectList("manager.searchManagerList",manager);
		return (ArrayList<Manager>)list;
	}

	public int idCheck(String id) {
		return (int) sqlSession.selectOne("manager.idCheck",id);
	}

	public int writeManagerInfo(Manager m) {
		return (int) sqlSession.insert("manager.writeManagerInfo",m);
	}

	public Manager selectOneManager(String userId) {
		return (Manager) sqlSession.selectOne("manager.selectOneManager",userId);
	}

	public int managerUpdateInfo(Manager m) {
		return (int) sqlSession.insert("manager.managerUpdateInfo",m);
	}

	public int countBoard() {
		return (int) sqlSession.selectOne("manager.countBoard");
	}

	public List<Manager> selectBoard(Manager m) {
		return (List<Manager>) sqlSession.selectList("manager.selectBoard",m);
	}
}
	