package com.ntels.portal.dao;

import java.util.ArrayList;
import java.util.List;

import org.mybatis.spring.SqlSessionTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.ntels.portal.domain.APIInfo;
import com.ntels.portal.domain.APIRequest;
import com.ntels.portal.domain.Manager;

@Repository
public class ApiMapper {
	@Autowired
	private SqlSessionTemplate sqlSession;
	public ArrayList<Manager> getNullCount() {
		List<Manager> groupManager = (List<Manager>) sqlSession.selectList("groupManager.groupManagersCount");
		return (ArrayList<Manager>)groupManager;
	}

}
