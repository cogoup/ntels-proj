package com.ntels.portal.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.ntels.portal.domain.APIInfo;
import com.ntels.portal.service.RestTemplateService;

import lombok.extern.slf4j.Slf4j;
import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

@Slf4j
@RestController
public class ApiController {
	private Logger logger = LoggerFactory.getLogger(this.getClass());
	@Autowired

	private final RestTemplateService restTemplateService;

	public ApiController(RestTemplateService restTemplateService) {
		this.restTemplateService = restTemplateService;
	}

	// client request
	@RequestMapping(value = "/api/user-count", method = RequestMethod.POST)
	public Map userCount(@RequestBody JSONObject responseJson) throws Exception {
		JSONArray jArray = responseJson.getJSONArray("data");
		APIInfo users = new APIInfo();
	    // 배열의 모든 아이템을 출력합니다.
	    for (int i = 0; i < jArray.size(); i++) {
	        JSONObject obj = jArray.getJSONObject(i);
	        String userGroupId = obj.getString("user_group_id");
	        String userGroupName = obj.getString("user_group_name");
	        int userCount = obj.getInt("user_count");
	        users.setUserGroupId(userGroupId);
	        users.setUserGroupName(userGroupName);
	        users.setUserCount(userCount);
	        return restTemplateService.userCount(users);
	    }
		return null;
	}
}
