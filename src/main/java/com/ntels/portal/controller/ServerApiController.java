package com.ntels.portal.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.ntels.portal.domain.APIInfo;
import com.ntels.portal.domain.APIRequest;
import com.ntels.portal.domain.Manager;
import com.ntels.portal.service.ApiService;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@RestController
@RequestMapping("/api/server")
public class ServerApiController {
	@Autowired
	private ApiService service;
	private Logger logger = LoggerFactory.getLogger(this.getClass());
    @PostMapping("/user-count")
    public Map<String, String> userCount(@RequestBody APIRequest group){
        Map<String, String> result = new HashMap<>();
		logger.info(group.toString());
		try { 
			APIInfo user = new APIInfo();
			user.setUserCount(group.getUserCount());
	        user.setUserGroupName(group.getUserGroupName());
	        user.setUserGroupId(group.getUserGroupId());
	        ArrayList<Manager> m = service.getNullCount();
	        for(Manager x : m) {
	        	if(x.getUserCount()==user.getUserCount()&&x.getUserGroupName().equals(user.getUserGroupName())) {
	        		result.put("code", "0");
	        		result.put("message", "Success");
	        		return result;
	        	}
	        }	
	        throw new Exception(); //강제 에러 출력
		} catch (Exception e) {
			e.printStackTrace();
			result.put("code", "1");
			result.put("message", "error");
		}
		return result;
    }


}