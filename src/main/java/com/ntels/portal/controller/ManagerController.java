package com.ntels.portal.controller;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.google.gson.Gson;
import com.ntels.portal.domain.Manager;
import com.ntels.portal.service.ManagerService;

@Controller
public class ManagerController {
	@Autowired
	private ManagerService service;
	private String thisUrl = "portal/manager";

	@RequestMapping(value = "/portal/managerList", method = RequestMethod.GET)
	public String ManagerList(Model model, HttpServletRequest request,Manager m, @RequestParam(value = "nowPage", required = false) String nowPage, @RequestParam(value = "cntPerPage", required = false) String cntPerPage) {
		try {
			ArrayList<Manager> managerList = service.ManagerList();
			ArrayList<Manager> groupList = service.groupList();
			int total = service.countBoard();
			if (nowPage == null && cntPerPage == null) {
				nowPage = "1";
				cntPerPage = "10";
			} else if (nowPage == null) {
				nowPage = "1";
			} else if (cntPerPage == null) {
				cntPerPage = "10";
			}
			
			Manager m1 = new Manager(total, Integer.parseInt(nowPage), Integer.parseInt(cntPerPage));
			List<Manager> view = service.selectBoard(m1);
			
			model.addAttribute("paging", m1);
			model.addAttribute("viewAll", view);
			
			
			model.addAttribute("list", managerList);
			model.addAttribute("gList", groupList);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return thisUrl + "/managerList";
	}

	@RequestMapping(value = "/portal/searchManagerList", method = RequestMethod.GET, produces = "application/json; charset=utf-8")
	@ResponseBody
	public String searchManagerList(Manager Manager) {
		ArrayList<Manager> searchList = service.searchManagerList(Manager);
		return new Gson().toJson(searchList);
	}

	@RequestMapping(value = "/portal/managerWrite", method = RequestMethod.GET )
	public String ManagerWrite(Model model, HttpServletRequest request) {
		ArrayList<Manager> groupList = service.groupList();
		model.addAttribute("gList", groupList);
		return thisUrl + "/managerWrite";
	}

	@RequestMapping(value = "/portal/idCheck", method = RequestMethod.POST)
	@ResponseBody
	public String idCheck(@RequestParam("userId") String id) {
		int manager = service.idCheck(id);
		if (manager == 1) {
			return "1";
		} else {
			return "0";
		}
	}

	@RequestMapping(value = "/portal/writeManagerInfo", method = RequestMethod.POST)
	public String writeManagerInfo(Manager m, Model model) {
		int result = service.writeManagerInfo(m);
		if (result > 0) {
			model.addAttribute("msg", "성공");
		} else {
			model.addAttribute("msg", "실패");
		}
		model.addAttribute("loc", "/portal/managerList");
		return "common/msg";
	}

	@RequestMapping(value = "/portal/managerUpdate", method = RequestMethod.GET)
	public String ManagerUpdate(Manager m, Model model, HttpServletRequest request) {
		Manager manager = service.selectOneManager(m.getUserId());
		ArrayList<Manager> groupList = service.groupList();
		model.addAttribute("gList", groupList);
		if (manager != null) {
			model.addAttribute("m", manager);
		}
		return thisUrl + "/managerUpdate";
	}

	@RequestMapping(value = "/portal/managerUpdateInfo", method = RequestMethod.POST)
	public String managerUpdateInfo(Manager m, Model model) {
		int result = service.managerUpdateInfo(m);
		if (result > 0) {
			model.addAttribute("msg", "성공");
		} else {
			model.addAttribute("msg", "실패");
		}
		model.addAttribute("loc", "/portal/managerList");
		return "common/msg";
	}
	
	
}
