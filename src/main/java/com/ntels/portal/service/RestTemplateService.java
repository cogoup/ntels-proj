package com.ntels.portal.service;

import java.net.URI;
import java.util.Map;

import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import com.ntels.portal.domain.APIInfo;
import com.ntels.portal.domain.APIRequest;

@Service
public class RestTemplateService {
    //response
    public Map userCount(APIInfo user){
  
       URI uri = UriComponentsBuilder
               .fromUriString("http://localhost:8080")
               .path("/api/server/user-count")
               .build().toUri();
  
        //uri : http://localhost:8080/api/server/user-count
      
        //http body -> object -> object mapper -> json -> rest template -> http body json
        APIRequest req = new APIRequest();
        req.setUserCount(user.getUserCount());
        req.setUserGroupId(user.getUserGroupId());
        req.setUserGroupName(user.getUserGroupName());
        RestTemplate restTemplate = new RestTemplate();
        //post 의 경우 PostForEntity를 사용한다. 파라미터 1 요청 주소 , 2 요청 바디 , 3 응답 바디
        ResponseEntity<Map> result = restTemplate.postForEntity(uri, req, Map.class);
        

        System.out.println(result.getStatusCode());
        System.out.println(result.getHeaders());
        System.out.println(result.getBody());

        return result.getBody();
    }
}