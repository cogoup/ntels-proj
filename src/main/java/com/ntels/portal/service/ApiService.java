package com.ntels.portal.service;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.stereotype.Service;

import com.ntels.portal.dao.ApiMapper;
import com.ntels.portal.domain.APIInfo;
import com.ntels.portal.domain.APIRequest;
import com.ntels.portal.domain.Manager;

@Service
public class ApiService {
	@Autowired
	private ApiMapper mapper;
	public ArrayList<Manager> getNullCount() {
		return mapper.getNullCount();
	}
}