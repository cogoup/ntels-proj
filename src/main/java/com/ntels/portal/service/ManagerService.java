package com.ntels.portal.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.ntels.portal.dao.ManagerMapper;
import com.ntels.portal.domain.Manager;

@Service
public class ManagerService {
	@Autowired
	private ManagerMapper mapper;
	
	public ArrayList<Manager> ManagerList() {
		return mapper.ManagerList();
	}

	public ArrayList<Manager> groupList() {
		return mapper.groupList();
	}

	public ArrayList<Manager> searchManagerList(Manager manager) {
		return mapper.searchManagerList(manager);
	}

	public int idCheck(String id) {
		return mapper.idCheck(id);
	}
	@Transactional
	public int writeManagerInfo(Manager m) {
		return mapper.writeManagerInfo(m);
	}

	public Manager selectOneManager(String userId) {
		return mapper.selectOneManager(userId);
	}
	@Transactional
	public int managerUpdateInfo(Manager m) {
		return mapper.managerUpdateInfo(m);
	}

	public int countBoard() {
		return mapper.countBoard();
	}

	public List<Manager> selectBoard(Manager m) {
		return mapper.selectBoard(m);
	}
}
